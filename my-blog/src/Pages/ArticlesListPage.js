import React, { Component } from 'react'
import ArticlesList from '../components/ArticlesList'
import articleContent from './article-content'

export class ArticlesListPage extends Component {
    render() {
        return (
            <div>
                <h1>Articles</h1>
                <ArticlesList articles={articleContent}/>
            </div>
        )
    }
}

export default ArticlesListPage
